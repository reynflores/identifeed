package com.example.healthawareness.Activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.MediaStore;

import com.example.healthawareness.Adapters.MealsAdapter;
import com.example.healthawareness.Fragments.HistoryFragment;
import com.example.healthawareness.Fragments.HomeFragment;
import com.example.healthawareness.Fragments.TipsFragment;
import com.example.healthawareness.Pojos.MealsObject;
import com.example.healthawareness.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.vision.label.internal.client.LabelOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.lzyzsd.circleprogress.DonutProgress;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.ml.common.FirebaseMLException;
import com.google.firebase.ml.common.modeldownload.FirebaseModelDownloadConditions;
import com.google.firebase.ml.common.modeldownload.FirebaseModelManager;
import com.google.firebase.ml.common.modeldownload.FirebaseRemoteModel;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.automl.FirebaseAutoMLLocalModel;
import com.google.firebase.ml.vision.automl.FirebaseAutoMLRemoteModel;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.label.FirebaseVisionImageLabel;
import com.google.firebase.ml.vision.label.FirebaseVisionImageLabeler;
import com.google.firebase.ml.vision.label.FirebaseVisionOnDeviceAutoMLImageLabelerOptions;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class HomeActivity extends AppCompatActivity {

    TextView textView_home, textView_history, textView_tips;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        initializeVariables();
        loadHome();
    }

    @Override
    public void onBackPressed() {

    }

    private void initializeVariables() {
        textView_home = findViewById(R.id.textView_home);
        textView_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadHome();
            }
        });

        textView_history = findViewById(R.id.textView_history);
        textView_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadHistory();
            }
        });

        textView_tips = findViewById(R.id.textView_tips);
        textView_tips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadTips();
            }
        });

        ImageView imageView_logout = findViewById(R.id.imageView_logout);
        imageView_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLogoutDialog();
            }
        });
    }

    public void loadHome() {
        textView_home.setTextColor(ContextCompat.getColor(this, R.color.white));
        textView_history.setTextColor(ContextCompat.getColor(this, R.color.bg_orange));
        textView_tips.setTextColor(ContextCompat.getColor(this, R.color.bg_orange));

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_frame, new HomeFragment()).commit();
    }

    public void loadHistory() {
        textView_history.setTextColor(ContextCompat.getColor(this, R.color.white));
        textView_home.setTextColor(ContextCompat.getColor(this, R.color.bg_orange));
        textView_tips.setTextColor(ContextCompat.getColor(this, R.color.bg_orange));

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_frame, new HistoryFragment()).commit();
    }

    public void loadTips() {
        textView_tips.setTextColor(ContextCompat.getColor(this, R.color.white));
        textView_history.setTextColor(ContextCompat.getColor(this, R.color.bg_orange));
        textView_home.setTextColor(ContextCompat.getColor(this, R.color.bg_orange));

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_frame, new TipsFragment()).commit();
    }

    public void showLogoutDialog() {
        final Dialog dialog = new Dialog(this, android.R.style.Theme_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_logout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Button button_cancel = dialog.findViewById(R.id.button_cancel);
        button_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button button_continue = dialog.findViewById(R.id.button_continue);
        button_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(HomeActivity.this, MainActivity.class));
                finish();
            }
        });

        dialog.show();
    }

}
