package com.example.healthawareness.Activities;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.healthawareness.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "";
    private FirebaseAuth mAuth;

    ProgressBar progressBar_loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initializeVariables();
    }

    private void initializeVariables() {
        mAuth = FirebaseAuth.getInstance();

        progressBar_loading = findViewById(R.id.progressBar_loading);
        progressBar_loading.setVisibility(View.INVISIBLE);

        final TextInputEditText text_input_email = findViewById(R.id.text_input_email);
        final TextInputEditText text_input_password = findViewById(R.id.text_input_password);

        Button button_complete = findViewById(R.id.button_complete);
        button_complete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!text_input_email.getText().toString().equals("") && !text_input_password.getText().toString().equals("")) {
                    progressBar_loading.setVisibility(View.VISIBLE);
                    loginUser(text_input_email.getText().toString(), text_input_password.getText().toString());
                } else {
                    Toast.makeText(LoginActivity.this, "Please enter needed fields.", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void loginUser(String email, String password) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            progressBar_loading.setVisibility(View.INVISIBLE);
                            startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                            finish();
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(LoginActivity.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                            progressBar_loading.setVisibility(View.INVISIBLE);
                        }
                    }
                });
    }

}
