package com.example.healthawareness.Activities;

import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.healthawareness.Adapters.ActivenessAdapter;
import com.example.healthawareness.Pojos.ActivenessObject;
import com.example.healthawareness.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ActivenessActivity extends AppCompatActivity {

    private static final String TAG = "";
    private FirebaseAuth mAuth;

    ArrayList<ActivenessObject> activeness_data = new ArrayList<>();
    ArrayList<ActivenessObject> selectedValues = new ArrayList<>();

    ProgressBar progressBar_loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activeness);

        initializeVariables();
    }

    private void initializeVariables() {
        mAuth = FirebaseAuth.getInstance();

        progressBar_loading = findViewById(R.id.progressBar_loading);
        progressBar_loading.setVisibility(View.INVISIBLE);

        final String gender = getIntent().getStringExtra("gender");
        final String age = getIntent().getStringExtra("age");
        final String weight = getIntent().getStringExtra("weight");
        final String height = getIntent().getStringExtra("height");
        final String name = getIntent().getStringExtra("name");
        final String email = getIntent().getStringExtra("email");
        final String password = getIntent().getStringExtra("password");

        Button button_proceed = findViewById(R.id.button_proceed);
        button_proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar_loading.setVisibility(View.VISIBLE);
                registerUser(gender, age, weight, height, name, email, password);
            }
        });

        activeness_data.add(new ActivenessObject(
                "1",
                "Sedentary lifestyle",
                "type of lifestyle involving little or no physical activity."));
        activeness_data.add(new ActivenessObject(
                "2",
                "Slightly active",
                "involves occasional physical activities but not consistent"));
        activeness_data.add(new ActivenessObject(
                "3",
                "Moderately active",
                "consists of walking 1.5 to 3 miles daily at a pace of 3 to 4 miles per hour"));
        activeness_data.add(new ActivenessObject(
                "4",
                "Active lifestyle",
                "way of life that integrates physical activity into your everyday routines, such as walking to the store or biking to work."));
        activeness_data.add(new ActivenessObject(
                "5",
                "Very active lifestyle",
                "People who love working out whether for cardio or weights. Are in the gym on average 5–7 times a week for 2–3hours."));

        updateRecyclerview(activeness_data);
    }

    public void updateRecyclerview(ArrayList<ActivenessObject> values) {
        ActivenessAdapter adapter = new ActivenessAdapter(values);
        RecyclerView mView = findViewById(R.id.recyclerview);
        mView.setNestedScrollingEnabled(false);
        mView.setHasFixedSize(false);
        mView.setAdapter(adapter);
        LinearLayoutManager llm = new LinearLayoutManager(ActivenessActivity.this);
        llm.setOrientation(RecyclerView.VERTICAL);
        mView.setLayoutManager(llm);

        adapter.setOnItemClickListener(new ActivenessAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                selectedValues.clear();
                selectedValues.add(activeness_data.get(position));
            }

            @Override
            public void onItemLongClick(int position, View v) {

            }
        });
    }

    public void registerUser(final String gender, final String age, final String weight,
                             final String height, final String name, String email, String password) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUser(
                                    user.getUid(),
                                    name,
                                    gender,
                                    Float.valueOf(age),
                                    Float.valueOf(weight),
                                    Float.valueOf(height));
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(ActivenessActivity.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                            progressBar_loading.setVisibility(View.INVISIBLE);
                        }
                    }
                });
    }

    public void updateUser(String id, String name, String gender, float age,
                           float weight, float height) {

        float recommended_calorie = 0;

        switch (gender) {
            case "Male":{
                recommended_calorie = (float) ((10 * weight) + (6.25 * height) - (5 * age) + 5);
                break;
            }
            case "Female":{
                recommended_calorie = (float) ((10 * weight) + (6.25 * height) - (5 * age) - 161);
                break;
            }
        }

        FirebaseFirestore db = FirebaseFirestore.getInstance();

        Map<String, Object> user = new HashMap<>();
        user.put("name", name);
        user.put("gender", gender);
        user.put("age", age);
        user.put("weight", weight);
        user.put("height", height);
        user.put("recommended_calorie", recommended_calorie);
        user.put("lifestyle", selectedValues.get(0).getId());

        db.collection("users").document(id)
                .set(user)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "DocumentSnapshot successfully written!");
                        progressBar_loading.setVisibility(View.INVISIBLE);
                        startActivity(new Intent(ActivenessActivity.this, HomeActivity.class));
                        finish();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(Exception e) {
                        Log.w(TAG, "Error writing document", e);
                        progressBar_loading.setVisibility(View.INVISIBLE);
                    }
                });

    }
}
