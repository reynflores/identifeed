package com.example.healthawareness.Activities;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.healthawareness.R;
import com.google.android.material.textfield.TextInputEditText;

public class RegistrationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        initializeVariables();
    }

    private void initializeVariables() {
        final TextInputEditText text_input_name = findViewById(R.id.text_input_name);
        final TextInputEditText text_input_email = findViewById(R.id.text_input_email);
        final TextInputEditText text_input_password = findViewById(R.id.text_input_password);

        Button button_complete = findViewById(R.id.button_complete);
        button_complete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegistrationActivity.this, BodyInformationActivity.class);
                intent.putExtra("name", text_input_name.getText().toString());
                intent.putExtra("email", text_input_email.getText().toString());
                intent.putExtra("password", text_input_password.getText().toString());
                startActivity(intent);
            }
        });
    }

}
