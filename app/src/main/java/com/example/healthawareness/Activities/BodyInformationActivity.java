package com.example.healthawareness.Activities;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.healthawareness.R;
import com.google.android.material.textfield.TextInputEditText;

public class BodyInformationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_body_information);

        initializeVariables();
    }

    private void initializeVariables() {
        final RadioGroup radioGroup = findViewById(R.id.radioGroup);

        final TextInputEditText text_input_age = findViewById(R.id.text_input_age);
        final TextInputEditText text_input_weight = findViewById(R.id.text_input_weight);
        final TextInputEditText text_input_height = findViewById(R.id.text_input_height);

        final String name = getIntent().getStringExtra("name");
        final String email = getIntent().getStringExtra("email");
        final String password = getIntent().getStringExtra("password");

        Button button_next = findViewById(R.id.button_next);
        button_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // get selected radio button from radioGroup
                int selectedId = radioGroup.getCheckedRadioButtonId();

                // find the radiobutton by returned id
                RadioButton radioButton = findViewById(selectedId);

                String gender = radioButton.getText().toString();

                Intent intent = new Intent(BodyInformationActivity.this, ActivenessActivity.class);
                intent.putExtra("gender", gender);
                intent.putExtra("age", text_input_age.getText().toString());
                intent.putExtra("weight", text_input_weight.getText().toString());
                intent.putExtra("height", text_input_height.getText().toString());
                intent.putExtra("name", name);
                intent.putExtra("email", email);
                intent.putExtra("password", password);
                startActivity(intent);
            }
        });
    }
}
