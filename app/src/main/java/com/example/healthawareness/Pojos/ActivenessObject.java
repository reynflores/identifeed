package com.example.healthawareness.Pojos;

import java.io.Serializable;

public class ActivenessObject implements Serializable {
    private String id, name, description;

    public ActivenessObject(String id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

}
