package com.example.healthawareness.Pojos;

public class MealsObject {
    private String image, name, description, calorie_count, created_at;

    public MealsObject(String image, String name, String description, String calorie_count, String created_at) {
        this.image = image;
        this.name = name;
        this.description = description;
        this.calorie_count = calorie_count;
        this.created_at = created_at;
    }

    public String getImage() {
        return image;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getCalorie_count() {
        return calorie_count;
    }

    public String getCreated_at() {
        return created_at;
    }
}
