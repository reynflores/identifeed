package com.example.healthawareness.Fragments;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.healthawareness.Activities.HomeActivity;
import com.example.healthawareness.Activities.MainActivity;
import com.example.healthawareness.Adapters.MealsAdapter;
import com.example.healthawareness.Pojos.MealsObject;
import com.example.healthawareness.R;
import com.github.lzyzsd.circleprogress.DonutProgress;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.ml.common.FirebaseMLException;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.automl.FirebaseAutoMLLocalModel;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.label.FirebaseVisionImageLabel;
import com.google.firebase.ml.vision.label.FirebaseVisionImageLabeler;
import com.google.firebase.ml.vision.label.FirebaseVisionOnDeviceAutoMLImageLabelerOptions;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

public class HomeFragment extends Fragment {
    private View view;
    private Context context;

    private static final String TAG = "";

    private static final String FILE_NAME = "temp.jpg";
    private static final int CAMERA_IMAGE_REQUEST = 3;
    private static final int GALLERY_IMAGE_REQUEST = 6;

    private TextView textView_recommended_calorie_intake, textView_calorie_intake;
    private DonutProgress donut_progress;

    private float recommended_calorie_intake;

    private Dialog dialog;

    private FirebaseAutoMLLocalModel localModel;
    private FirebaseVisionImageLabeler labeler;

    private FirebaseUser currentUser;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);

        localModel = new FirebaseAutoMLLocalModel.Builder()
                .setAssetFilePath("manifest.json")
                .build();
        try {
            FirebaseVisionOnDeviceAutoMLImageLabelerOptions options =
                    new FirebaseVisionOnDeviceAutoMLImageLabelerOptions.Builder(localModel)
                            .setConfidenceThreshold(0.5f)
                            .build();
            labeler = FirebaseVision.getInstance().getOnDeviceAutoMLImageLabeler(options);
        } catch (FirebaseMLException e) {
            e.printStackTrace();
        }

        initializeVariables();

        return view;
    }

    private void initializeVariables() {
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();

        textView_recommended_calorie_intake = view.findViewById(R.id.textView_recommended_calorie_intake);

        final FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference userRef = db.collection("users").document(currentUser.getUid());
        userRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        recommended_calorie_intake = Float.valueOf(document.getData().get("recommended_calorie").toString());
                        textView_recommended_calorie_intake.setText(String.valueOf(recommended_calorie_intake));

                        getMealsData(db);

                        Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                    } else {
                        Log.d(TAG, "No such document");
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                }
            }
        });

        textView_calorie_intake = view.findViewById(R.id.textView_calorie_intake);

        donut_progress = view.findViewById(R.id.donut_progress);

        FloatingActionButton fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] colors = {"Take image using camera", "Select image from the gallery"};

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Pick an action");
                builder.setItems(colors, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // the user clicked on colors[which]
                        if (which == 0) {
                            startCamera();
                        }
                        if (which == 1) {
                            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                            photoPickerIntent.setType("image/*");
                            startActivityForResult(photoPickerIntent, GALLERY_IMAGE_REQUEST);
                        }
                    }
                });
                builder.show();
            }
        });
    }

    private void getMealsData(FirebaseFirestore db) {
        final ArrayList<MealsObject> meals_data = new ArrayList<>();
        db.collection("meals")
                .whereEqualTo("user_id", currentUser.getUid())
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        for (QueryDocumentSnapshot queryDocumentSnapshot : queryDocumentSnapshots) {
                            meals_data.add(new MealsObject(queryDocumentSnapshot.get("image").toString(),
                                    queryDocumentSnapshot.get("name").toString(),
                                    queryDocumentSnapshot.get("description").toString(),
                                    queryDocumentSnapshot.get("calorie_count").toString(),
                                    queryDocumentSnapshot.get("created_at").toString()));
                        }

                        sortMealsData(meals_data);
                    }
                });
    }

    private void sortMealsData(ArrayList<MealsObject> values) {
        ArrayList<MealsObject> todays_meals = new ArrayList<>();

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("MM-dd-yyyy");
        String formattedDate = df.format(c);

        for (int i = 0; i < values.size(); i++) {
            if (getDate(Long.parseLong(values.get(i).getCreated_at())).equals(formattedDate)) {
                todays_meals.add(values.get(i));
            }
        }

        if (!todays_meals.isEmpty()) {
            getCaloriePercentage(todays_meals);

            Collections.sort(todays_meals, new Comparator<MealsObject>() {
                public int compare(MealsObject o1, MealsObject o2) {
                    return o1.getCreated_at().compareTo(o2.getCreated_at());
                }
            });

            ArrayList<MealsObject> latest_meal = new ArrayList<>();
            latest_meal.add(todays_meals.get(todays_meals.size() - 1));
            updateRecyclerview(latest_meal);
        }

    }

    private String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time * 1000);
        return DateFormat.format("MM-dd-yyyy", cal).toString();
    }

    private void getCaloriePercentage(ArrayList<MealsObject> values) {
        if (!values.isEmpty()) {
            float daily_calorie_intake = 0;
            for (int i = 0; i < values.size(); i++) {
                daily_calorie_intake = daily_calorie_intake + Float.valueOf(values.get(i).getCalorie_count());
            }
            textView_calorie_intake.setText(String.valueOf(daily_calorie_intake));
            int percentage = (int) ((daily_calorie_intake / recommended_calorie_intake) * 100);
            updateDonut(donut_progress, percentage);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAMERA_IMAGE_REQUEST && resultCode == RESULT_OK) {
            try {
                Uri photoUri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", getCameraFile());
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), photoUri);

                FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(bitmap);
                runDetector(image);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (requestCode == GALLERY_IMAGE_REQUEST && resultCode == RESULT_OK) {
            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = context.getContentResolver().openInputStream(imageUri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);

                FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(selectedImage);
                runDetector(image);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }

    }

    private void runDetector(FirebaseVisionImage image) {
        dialog = new Dialog(context, android.R.style.Theme_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_processing);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();

        labeler.processImage(image)
                .addOnSuccessListener(new OnSuccessListener<List<FirebaseVisionImageLabel>>() {
                    @Override
                    public void onSuccess(List<FirebaseVisionImageLabel> labels) {
                        String text_labels = "";
                        for (FirebaseVisionImageLabel label: labels) {
                            text_labels = label.getText();
                            float confidence = label.getConfidence();
                        }
                        recognizeImage(text_labels);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        e.printStackTrace();
                    }
                });
    }

    private void recognizeImage(String label) {
        if (!label.equals("")) {
            System.out.println("Label : " + label);
            final FirebaseFirestore db = FirebaseFirestore.getInstance();
            db.collection("foods")
                    .whereEqualTo("name", label)
                    .get()
                    .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                        @Override
                        public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                            for (QueryDocumentSnapshot queryDocumentSnapshot : queryDocumentSnapshots) {

                                Map<String, Object> meal = new HashMap<>();
                                meal.put("calorie_count", queryDocumentSnapshot.get("calorie_count").toString());
                                meal.put("created_at", String.valueOf(System.currentTimeMillis() / 1000));
                                meal.put("description", queryDocumentSnapshot.get("description").toString());
                                meal.put("image", queryDocumentSnapshot.get("image").toString());
                                meal.put("name", queryDocumentSnapshot.get("name").toString());
                                meal.put("user_id", currentUser.getUid());

                                showFoodInformation(meal);
                            }

                            dialog.dismiss();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            e.printStackTrace();
                        }
                    });
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Can't recognize image.", Toast.LENGTH_SHORT).show();
        }

    }

    private void showFoodInformation(final Map<String, Object> meal) {
        final FirebaseFirestore db = FirebaseFirestore.getInstance();

        final Dialog dialog_information = new Dialog(context, android.R.style.Theme_Dialog);
        dialog_information.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_information.setContentView(R.layout.dialog_information);
        dialog_information.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView imageView = dialog_information.findViewById(R.id.imageView);
        Picasso.get()
                .load(meal.get("image").toString())
                .fit()
                .centerCrop()
                .into(imageView);

        TextView textView_name = dialog_information.findViewById(R.id.textView_name);
        String name = "Name: " + meal.get("name").toString();
        textView_name.setText(name);

        TextView textView_description = dialog_information.findViewById(R.id.textView_description);
        String description = "Description: " + meal.get("description").toString();
        textView_description.setText(description);

        TextView textView_calorie = dialog_information.findViewById(R.id.textView_calorie);
        String calorie_count = "Calorie count: " + meal.get("calorie_count").toString() + "g";
        textView_calorie.setText(calorie_count);

        Button button_cancel = dialog_information.findViewById(R.id.button_cancel);
        button_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_information.dismiss();
            }
        });

        Button button_confirm = dialog_information.findViewById(R.id.button_confirm);
        button_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = db.collection("meals").document().getId();
                db.collection("meals").document(id)
                        .set(meal)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                getMealsData(db);
                                Log.d(TAG, "DocumentSnapshot successfully written!");
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(Exception e) {
                                Log.w(TAG, "Error writing document", e);
                            }
                        });
                dialog_information.dismiss();
            }
        });

        dialog_information.show();
    }

    private void startCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Uri photoUri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", getCameraFile());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(intent, CAMERA_IMAGE_REQUEST);
    }

    private File getCameraFile() {
        File dir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return new File(dir, FILE_NAME);
    }

    private void updateDonut(final DonutProgress donut_progress, int progress) {
        if (progress >= 100) {
            progress = 100;
        }
        if (progress >= 100){
            donut_progress.setFinishedStrokeColor(ContextCompat.getColor(context, R.color.red));
            donut_progress.setUnfinishedStrokeColor(ContextCompat.getColor(context, R.color.bg_red));
            donut_progress.setTextColor(ContextCompat.getColor(context, R.color.red));
        } else {
            donut_progress.setFinishedStrokeColor(ContextCompat.getColor(context, R.color.green));
            donut_progress.setUnfinishedStrokeColor(ContextCompat.getColor(context, R.color.bg_green));
            donut_progress.setTextColor(ContextCompat.getColor(context, R.color.green));
        }

        if (progress != 0){
            final int finalProgress = progress;
            new CountDownTimer(2500, 10) {
                float x = 0;
                public void onTick(long millisUntilFinished) {
                    if (donut_progress.getProgress() != finalProgress){
                        donut_progress.setProgress(x);
                        x++;
                    }
                }
                public void onFinish() {

                }
            }.start();
        }
    }

    private void updateRecyclerview(ArrayList<MealsObject> values) {
        MealsAdapter adapter = new MealsAdapter(values);
        RecyclerView mView = view.findViewById(R.id.recyclerview);
        mView.setNestedScrollingEnabled(false);
        mView.setHasFixedSize(false);
        mView.setAdapter(adapter);
        LinearLayoutManager llm = new LinearLayoutManager(context);
        llm.setOrientation(RecyclerView.VERTICAL);
        mView.setLayoutManager(llm);

        adapter.setOnItemClickListener(new MealsAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {

            }

            @Override
            public void onItemLongClick(int position, View v) {

            }
        });
    }
}