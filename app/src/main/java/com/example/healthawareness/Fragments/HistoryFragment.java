package com.example.healthawareness.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.healthawareness.Adapters.MealsHistoryAdapter;
import com.example.healthawareness.Pojos.MealsObject;
import com.example.healthawareness.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class HistoryFragment extends Fragment {
    private View view;
    private Context context;

    private FirebaseUser currentUser;

    public HistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_history, container, false);

        initializeVariables();

        return view;
    }

    private void initializeVariables() {
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();
        final FirebaseFirestore db = FirebaseFirestore.getInstance();

        getMealsData(db);
    }

    private void getMealsData(FirebaseFirestore db) {
        final ArrayList<MealsObject> meals_data = new ArrayList<>();
        db.collection("meals")
                .whereEqualTo("user_id", currentUser.getUid())
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        for (QueryDocumentSnapshot queryDocumentSnapshot : queryDocumentSnapshots) {
                            meals_data.add(new MealsObject(queryDocumentSnapshot.get("image").toString(),
                                    queryDocumentSnapshot.get("name").toString(),
                                    queryDocumentSnapshot.get("description").toString(),
                                    queryDocumentSnapshot.get("calorie_count").toString(),
                                    queryDocumentSnapshot.get("created_at").toString()));
                        }

                        Collections.sort(meals_data, new Comparator<MealsObject>() {
                            public int compare(MealsObject o1, MealsObject o2) {
                                return o1.getCreated_at().compareTo(o2.getCreated_at());
                            }
                        });
                        Collections.reverse(meals_data);
                        updateRecyclerview(meals_data);
                    }
                });
    }

    private void updateRecyclerview(ArrayList<MealsObject> values) {
        MealsHistoryAdapter adapter = new MealsHistoryAdapter(values);
        RecyclerView mView = view.findViewById(R.id.recyclerview);
        mView.setNestedScrollingEnabled(false);
        mView.setHasFixedSize(false);
        mView.setAdapter(adapter);
        LinearLayoutManager llm = new LinearLayoutManager(context);
        llm.setOrientation(RecyclerView.VERTICAL);
        mView.setLayoutManager(llm);

        adapter.setOnItemClickListener(new MealsHistoryAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {

            }

            @Override
            public void onItemLongClick(int position, View v) {

            }
        });
    }
}