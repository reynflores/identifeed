package com.example.healthawareness.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.healthawareness.Pojos.MealsObject;
import com.example.healthawareness.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;

public class TipsFragment extends Fragment {
    private View view;
    private Context context;

    TextView textView_title, textView_description;
    Button button_next;

    FirebaseFirestore db;

    public TipsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_tips, container, false);

        initializeVariables();
        getTriviasData(db);
        
        return view;
    }

    private void initializeVariables() {
        db = FirebaseFirestore.getInstance();

        textView_title = view.findViewById(R.id.textView_title);
        textView_description = view.findViewById(R.id.textView_description);

        button_next = view.findViewById(R.id.button_next);
        button_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getTriviasData(db);
            }
        });
    }

    private void getTriviasData(FirebaseFirestore db) {
        final int min = 1;
        final int max = 10;
        final int random = new Random().nextInt((max - min) + 1) + min;
        db.collection("trivias")
                .whereEqualTo("id", random+"")
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        for (QueryDocumentSnapshot queryDocumentSnapshot : queryDocumentSnapshots) {
                            textView_title.setText(queryDocumentSnapshot.get("title").toString());
                            textView_description.setText(queryDocumentSnapshot.get("description").toString());
                        }
                        
                    }
                });
    }
}