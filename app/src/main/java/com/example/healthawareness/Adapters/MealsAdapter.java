package com.example.healthawareness.Adapters;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.healthawareness.Pojos.MealsObject;
import com.example.healthawareness.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MealsAdapter extends RecyclerView.Adapter<MealsAdapter.MyViewHolder> {
    private static ClickListener clickListener;

    public ArrayList<MealsObject> myValues;

    public MealsAdapter(ArrayList<MealsObject> myValues) {
        this.myValues = myValues;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listcategory = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_meals, parent, false);
        return new MyViewHolder(listcategory);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        Picasso.get()
                .load(myValues.get(position).getImage())
                .fit()
                .centerCrop()
                .into(holder.imageView);

        holder.textView_name.setText(myValues.get(position).getName());
        holder.textView_description.setText(myValues.get(position).getDescription());
        holder.textView_calorieCount.setText(myValues.get(position).getCalorie_count());
    }

    @Override
    public int getItemCount() {
        return myValues.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        ImageView imageView;
        TextView textView_name, textView_description, textView_calorieCount;
        public MyViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

            imageView = itemView.findViewById(R.id.imageView);
            textView_name = itemView.findViewById(R.id.textView_name);
            textView_description = itemView.findViewById(R.id.textView_description);
            textView_calorieCount = itemView.findViewById(R.id.textView_calorieCount);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(),v);
        }

        @Override
        public boolean onLongClick(View v) {
            clickListener.onItemLongClick(getAdapterPosition(),v);
            return false;
        }
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        MealsAdapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
        void onItemLongClick(int position, View v);
    }
}
