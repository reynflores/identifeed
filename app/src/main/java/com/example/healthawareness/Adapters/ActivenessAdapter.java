package com.example.healthawareness.Adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.healthawareness.Pojos.ActivenessObject;
import com.example.healthawareness.R;

import java.util.ArrayList;

public class ActivenessAdapter extends RecyclerView.Adapter<ActivenessAdapter.MyViewHolder> {
    private static ClickListener clickListener;

    static Context context;
    private ArrayList<ActivenessObject> myValues;

    private int selectedPos = RecyclerView.NO_POSITION;

    public ActivenessAdapter(ArrayList<ActivenessObject> myValues) {
        this.myValues = myValues;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listcategory = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_activeness, parent, false);
        context = parent.getContext();
        return new MyViewHolder(listcategory);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.itemView.setSelected(selectedPos == position);
        holder.textView_name.setText(myValues.get(position).getName());
        holder.textView_description.setText(myValues.get(position).getDescription());
    }

    @Override
    public int getItemCount() {
        return myValues.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        TextView textView_name, textView_description;

        public MyViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

            textView_name = itemView.findViewById(R.id.textView_name);
            textView_description = itemView.findViewById(R.id.textView_description);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(),v);
            notifyItemChanged(selectedPos);
            selectedPos = getLayoutPosition();
            notifyItemChanged(selectedPos);
        }

        @Override
        public boolean onLongClick(View v) {
            clickListener.onItemLongClick(getAdapterPosition(),v);
            return false;
        }
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        ActivenessAdapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
        void onItemLongClick(int position, View v);
    }

}
