package com.example.healthawareness.Adapters;

import androidx.recyclerview.widget.RecyclerView;

import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.healthawareness.Pojos.MealsObject;
import com.example.healthawareness.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class MealsHistoryAdapter extends RecyclerView.Adapter<MealsHistoryAdapter.MyViewHolder> {
    private static ClickListener clickListener;

    public ArrayList<MealsObject> myValues;

    public MealsHistoryAdapter(ArrayList<MealsObject> myValues) {
        this.myValues = myValues;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listcategory = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_meals_history, parent, false);
        return new MyViewHolder(listcategory);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        Picasso.get()
                .load(myValues.get(position).getImage())
                .fit()
                .centerCrop()
                .into(holder.imageView);

        holder.textView_name.setText(myValues.get(position).getName());
        holder.textView_calorieCount.setText(myValues.get(position).getCalorie_count());
        holder.textView_date.setText(getDate(Long.valueOf(myValues.get(position).getCreated_at())));
    }

    private String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time * 1000);
        return DateFormat.format("MM-dd-yyyy HH-mm", cal).toString();
    }

    @Override
    public int getItemCount() {
        return myValues.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        ImageView imageView;
        TextView textView_name, textView_description, textView_calorieCount, textView_date;
        public MyViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

            imageView = itemView.findViewById(R.id.imageView);
            textView_name = itemView.findViewById(R.id.textView_name);
            textView_calorieCount = itemView.findViewById(R.id.textView_calorieCount);
            textView_date = itemView.findViewById(R.id.textView_date);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(),v);
        }

        @Override
        public boolean onLongClick(View v) {
            clickListener.onItemLongClick(getAdapterPosition(),v);
            return false;
        }
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        MealsHistoryAdapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
        void onItemLongClick(int position, View v);
    }
}
